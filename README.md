# tailwind

Ce projet est un projet de base pour utiliser tailwind CSS sans avoir à utiliser un framework. Il est très facile à utiliser, il suffit juste de comprendre quelques commandes et de configurer le fichier tailwind.config.js.

Pour l'installer, il suffit de faire la commande suivante:

```bash
npm install
```

Pour l'utiliser, il suffit de faire la commande suivante:

```bash
npm run dev
```

ou

```bash
npm start
```
